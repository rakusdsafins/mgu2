W repozytorium znajduje się kod ułatwiający współpracę z danymi na potrzeby konkursu https://www.kaggle.com/c/cifar-10/. 
### Jupyter Notebooki zawierają kod tworzący sieci neuronowe użyte w konkursie: ###
* https://colab.research.google.com/drive/1FzoGx9ru6Gu5QnuDi7RwmIFb32KH_Xit?usp=sharing
* https://colab.research.google.com/drive/10QvyXdvPyo5h0QOP3h2ctolvuGLwzJIh?usp=sharing
* https://colab.research.google.com/drive/1n7s_Q4sKvXCpAdPxpCdZ2vDD1zaYtUX9?usp=sharing
* https://colab.research.google.com/drive/1TVpKdIZkftQnENAd6QpJIYI88UPZiWs_?usp=sharing
* https://colab.research.google.com/drive/1T-WlwS5TSZHGKn92gJ9P0QJwT8UBkHE8?usp=sharing
    
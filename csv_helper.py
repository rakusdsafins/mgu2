import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

class CsvHelper:
    def __init__(self):
        self.labels_classes_dict = {
            'frog': 0,
            'truck': 1,
            'deer': 2,
            'automobile': 3,
            'bird': 4,
            'horse': 5,
            'ship': 6,
            'cat': 7,
            'dog': 8,
            'airplane': 9
        }

        self.classes_labels_dict = { v: k for k, v 
            in self.labels_classes_dict.items()}

    def read_csv(self, path, sep=','):
        self.df = pd.read_csv(path, sep=sep)
        self.create_class_col()

    def create_class_col(self):
        self.df['class'] = self.df['label'].map(self.labels_classes_dict)

    def split_data(self, images, train_frac = 0.8):
        assert len(self.df['class'] == len(images))

        train_images,test_images,train_labels,test_labels = train_test_split(
            images, self.df['class'].values, train_size=train_frac)

        return (train_images, test_images), (train_labels, test_labels)

    def save_predictions(self, predicted, path='predicted.csv'):
        df = pd.DataFrame({
            'id': np.arange(1, 1 + len(predicted)),
            'label': predicted
        })

        df['label'] = df['label'].map(self.classes_labels_dict)

        df.to_csv(path, index=False)

    def get_classes(self):
        return self.df['class'].values

    def split_image_and_classes(self, images, classes, fraction=0.8, shuffle=True):
        return train_test_split(
            images, classes, train_size=fraction, shuffle=shuffle)

    def create_copy(self, np_array):
        return np.copy(np_array)

    def join_arrays(self, *args):
        return np.concatenate(tuple(args))

import numpy as np
import cv2
import imutils
from keras.preprocessing.image import ImageDataGenerator

class ImageHelper:
    def read_images(self, path = 'data/train', count = 50_000, color = True):
        images = np.array([cv2.imread(f"{path}/{i}.png", color)
            for i in range(1, count + 1)])
        
        if color:
            images = np.array([cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                for img in images])

        return images

    def to_grayscale(self, images):
        gray_images = np.array([cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            for img in images])

        return gray_images

    def normalize_images(self, images):
        return images / 255

    def smooth_images(self, images, filter = (5,5)):
        smoothed_images = np.array([cv2.GaussianBlur(img, filter, 0)
            for img in images])

        return smoothed_images

    def random_zoom_images(self, images, stretch_shape, output_shape = None):
        stretched_images = self._stretch(images, stretch_shape)
        
        if output_shape == None:
            output_shape = (images.shape[1], images.shape[2]) 
        zoomed_images = self._zoom_in(stretched_images, output_shape)

        return zoomed_images

    def rotate_images(self, images, angle = 90):
        rotated_images = np.array([imutils.rotate(img, angle)
            for img in images])

        return rotated_images

    def random_rotate_images(self, images, max_angle = 90):
        rotated_images = np.array([imutils.rotate(img,
         np.random.randint(max_angle)) for img in images])

        return rotated_images

    def _zoom_in(self, images, zoom_shape):
        # we read x,y of images
        count = images.shape[0]
        img_shape = images.shape[1:3]
        x_diff = img_shape[0] - zoom_shape[0]
        y_diff = img_shape[1] - zoom_shape[1]

        assert x_diff >= 0 and y_diff >= 0
        x_offsets = np.random.randint(0, x_diff + 1, size=count)
        y_offsets = np.random.randint(0, y_diff + 1, size=count)

        zoomed_images = np.array([
            img[x_offsets[i]:zoom_shape[0] + x_offsets[i],
            y_offsets[i]:zoom_shape[1] + y_offsets[i],
            :] 
            for i, img in enumerate(images)])

        return zoomed_images


    def _stretch(self, images, stretch_shape):
        stretched_images = np.array([cv2.resize(img, stretch_shape) 
            for img in images])

        return stretched_images
    
    # Returns tuple where first item is numpy array of mutated images and second one is numpy array of labels
    def apply_keras_modifications(self, images, labels, **kwargs):
        print("Keyword arguments https://keras.io/preprocessing/image/#imagedatagenerator-methods")
        generator = ImageDataGenerator(**kwargs)

        gen = generator.flow(images, labels, shuffle=False,
            batch_size=images.shape[0])

        return next(gen)




        

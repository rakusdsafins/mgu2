from keras.models import load_model, Model, Input
import keras.layers

class ModelHelper:
    # creates ensembleModel that you can call .predict() on
    def create_ensemble_models(self, paths: list):
        models = []
        for path in paths:
            models.append(load_model(path))

        ensembleModel = self._merge_models(models)
        return ensembleModel

    def _merge_models(self, models):

        model_input = Input(shape=models[0].input_shape[1:])
        outputs = [model(model_input) for model in models]
        outputAverage = keras.layers.average(outputs)

        ensembleModel = Model(inputs = model_input,
            outputs = outputAverage, name='ensemble')

        return ensembleModel
        
    # path should end with .h5
    def save_model(self, model, path):
        model.save(path)